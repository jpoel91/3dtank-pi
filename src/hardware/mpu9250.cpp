#include <iostream>
#include <stdio.h>
#include "mpu9250.h"

using std::cout, std::endl;



Mpu9250::Mpu9250(uint8_t gyro_acc_addr) {
    this->gyro_acc = new IICCon(gyro_acc_addr);
    this->mag = NULL;
    calibrationValue = {0};
    noiseFilter = {0};
    angle = {0};

}

Mpu9250::~Mpu9250() {
    delete gyro_acc;
    delete mag;
}

void Mpu9250::Init() {
    /* Set CTRL_REG1 (0x20)
    ====================================================================
    BIT  Symbol    Description                                   Default
    ---  ------    --------------------------------------------- -------
    
    */
}

void Mpu9250::Calibrate() {

}

void Mpu9250::Shutdown() {

}

void Mpu9250::Boot() {

}

int32_t int16to32(int16_t num){

}

void Mpu9250::Measure() {

}

void Mpu9250::ResetFIFO() {

}