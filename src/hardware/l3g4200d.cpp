#include "l3g4200d.h"
#include <cmath>
#include <signal.h>


using std::cout;
using std::abs;

L3g4200d::L3g4200d() {
    i2c = new IICCon(L3G4200D_ADDRESS);

    if (i2c->readRegister(L3G4200D_REG_WHO_AM_I) != 0xD3) {
        cout << "L3g4200d not found on I2C bus..\n";
        throw L3G4200D_NOT_FOUND;
        delete this;
    }
    calibrationValue = {0};
    noiseFilter = {0};

}

L3g4200d::~L3g4200d() {
    delete i2c;
    cout << "Closed gyro\n";
}

void L3g4200d::Init() {
    /* Set CTRL_REG1 (0x20)
    ====================================================================
    BIT  Symbol    Description                                   Default
    ---  ------    --------------------------------------------- -------
    7-6  DR1/0     Output data rate                                   00
    5-4  BW1/0     Bandwidth selection                                00
    3    PD        0 = Power-down mode, 1 = normal/sleep mode          0
    2    ZEN       Z-axis enable (0 = disabled, 1 = enabled)           1
    1    YEN       Y-axis enable (0 = disabled, 1 = enabled)           1
    0    XEN       X-axis enable (0 = disabled, 1 = enabled)           1 */
     
    //Poweroff and reenable the gyro.
    i2c->writeRegister(L3G4200D_REG_CTRL_REG1, L3G4200D_SHUTDOWN);
    
    i2c->writeRegister(L3G4200D_REG_CTRL_REG1, \
        L3G4200D_800HZ_110CO | L3G4200D_ENABLE_ALL_AXIS);
    

    /* Set CTRL_REG2 (0x21)
    ====================================================================
    BIT  Symbol    Description                                   Default
    ---  ------    --------------------------------------------- -------
    5-4  HPM1/0    High-pass filter mode selection                    00
    3-0  HPCF3..0  High-pass filter cutoff frequency selection      0000 */

    //i2c->writeRegister(L3G4200D_REG_CTRL_REG2, 0x00);

    /* Set CTRL_REG3 (0x22)
    ====================================================================
    BIT  Symbol    Description                                   Default
    ---  ------    --------------------------------------------- -------
    7    I1_Int1   Interrupt enable on INT1 (0=disable,1=enable)       0
    6    I1_Boot   Boot status on INT1 (0=disable,1=enable)            0
    5    H-Lactive Interrupt active config on INT1 (0=high,1=low)      0
    4    PP_OD     Push-Pull/Open-Drain (0=PP, 1=OD)                   0
    3    I2_DRDY   Data ready on DRDY/INT2 (0=disable,1=enable)        0
    2    I2_WTM    FIFO wtrmrk int on DRDY/INT2 (0=dsbl,1=enbl)        0
    1    I2_ORun   FIFO overrun int on DRDY/INT2 (0=dsbl,1=enbl)       0
    0    I2_Empty  FIFI empty int on DRDY/INT2 (0=dsbl,1=enbl)         0 */

    //i2c->writeRegister(L3G4200D_REG_CTRL_REG3, 0x00);

    /* Set CTRL_REG4 (0x23)
    ====================================================================
    BIT  Symbol    Description                                   Default
    ---  ------    --------------------------------------------- -------
    7    BDU       Block Data Update (0=continuous, 1=LSB/MSB)         0
    6    BLE       Big/Little-Endian (0=Data LSB, 1=Data MSB)          0
    5-4  FS1/0     Full scale selection                               00
                                00 = 250 dps
                                01 = 500 dps
                                10 = 2000 dps
                                11 = 2000 dps
    0    SIM       SPI Mode (0=4-wire, 1=3-wire)                       0 */    

    i2c->writeRegister(L3G4200D_REG_CTRL_REG4, 0x20);

    /* Set CTRL_REG5 (0x24)
    ====================================================================
    BIT  Symbol    Description                                   Default
    ---  ------    --------------------------------------------- -------
    7    BOOT      Reboot memory content (0=normal, 1=reboot)          0
    6    FIFO_EN   FIFO enable (0=FIFO disable, 1=enable)              0
    4    HPen      High-pass filter enable (0=disable,1=enable)        0
    3-2  INT1_SEL  INT1 Selection config                              00
    1-0  OUT_SEL   Out selection config                               00 */

    i2c->writeRegister(L3G4200D_REG_CTRL_REG5, 0x40);

    this->Shutdown();
    usleep(250 * 1000);
}

void L3g4200d::Calibrate() {
    int numCal = 0;
    uint8_t numMeasurements = 0;
    Vector_t highest = {0};
    cout << "Dont move! Calibration is running!\n";
    
    this->Boot();

    while (numCal < L3G4200D_NUM_CAL) {
        numMeasurements = i2c->readRegister(L3G4200D_REG_FIFO_SRC_REG) & 31;
        for (uint8_t i = 0; i < numMeasurements; i++, numCal++) {
            Vector_t temp;
            uint8_t* measurement = i2c->readRegister(L3G4200D_REG_OUT_X_L    \
                | L3G4200D_INCREMENTAL_READ, 6);

            //Calculate x, y and z.    
            temp.x = measurement[x_l] | (measurement[x_h] << 8);
            temp.y = measurement[y_l] | (measurement[y_h] << 8);
            temp.z = measurement[z_l] | (measurement[z_h] << 8); 

            if (abs(temp.x) > highest.x) highest.x = abs(temp.x);
            if (abs(temp.y) > highest.y) highest.y = abs(temp.y);
            if (abs(temp.z) > highest.z) highest.z = abs(temp.z);

            // //Add to the calibration value.
            calibrationValue.x += temp.x;
            calibrationValue.y += temp.y;
            calibrationValue.z += temp.z;
           
            free(measurement);
        }
    }

    printf("Before calibration:\n");
    printf("x:(%d), y:(%d), z:(%d)\n", calibrationValue.x, calibrationValue.y, calibrationValue.z);

    calibrationValue.x /= numCal;
    calibrationValue.y /= numCal;
    calibrationValue.z /= numCal;

    printf("After calibration:\n");
    printf("x:(%d), y:(%d), z:(%d)\n", calibrationValue.x, calibrationValue.y, calibrationValue.z);

    noiseFilter.x = 0.004 + ((calibrationValue.x) * L3G4200D_SCALE_500DPS * 0.00125);
    noiseFilter.y = 0.004 + ((calibrationValue.y) * L3G4200D_SCALE_500DPS * 0.00125);
    noiseFilter.z = 0.004 + ((calibrationValue.z) * L3G4200D_SCALE_500DPS * 0.00125);

    printf("Noise: \nx: %lf\ny: %lf\nz: %lf\n", noiseFilter.x, noiseFilter.y, noiseFilter.z);

    this->Shutdown();
}

void L3g4200d::Shutdown() {
    i2c->writeRegister(L3G4200D_REG_CTRL_REG1, L3G4200D_SHUTDOWN);
}

void L3g4200d::Boot() {
    usleep(250 * 1000);
    i2c->writeRegister(L3G4200D_REG_CTRL_REG1, \
        L3G4200D_800HZ_110CO | L3G4200D_ENABLE_ALL_AXIS);

    this->ResetFIFO();
}

int32_t L3g4200d::int16to32(int16_t num){
	int32_t newNum = 0;
	int32_t sign = num & 0x8000;
	num = num & 0x7FFF;
	newNum = num;
	if(sign) newNum -= 0x8000;
	return newNum;
}

void L3g4200d::Measure() {

    while (1) {
        uint8_t numMeasurements = i2c->readRegister(L3G4200D_REG_FIFO_SRC_REG) & 31;
        Vector_t angle_raw = {0};
        int i;
        for (i = 0; i < numMeasurements; i++) {
            uint8_t* measurement = i2c->readRegister(L3G4200D_REG_OUT_X_L \
            | L3G4200D_INCREMENTAL_READ, 6);
            
            Vector_t tempVal;
            DVector_t temp;

            tempVal.x = measurement[x_l] | (measurement[x_h] << 8);
            tempVal.y = measurement[y_l] | (measurement[y_h] << 8);
            tempVal.z = measurement[z_l] | (measurement[z_h] << 8);  

            //printf("%d\n", tempVal.x);
            
            angle_raw.x = int16to32(tempVal.x - calibrationValue.x);
            angle_raw.y = int16to32(tempVal.y - calibrationValue.y);
            angle_raw.z = int16to32(tempVal.z - calibrationValue.z);
            
            temp.x = angle_raw.x * L3G4200D_SCALE_2000DPS * 0.00125; 
            temp.y = angle_raw.y * L3G4200D_SCALE_2000DPS * 0.00125; 
            temp.z = angle_raw.z * L3G4200D_SCALE_2000DPS * 0.00125; 

            angle.x += temp.x;  //degrees     
            angle.y += temp.y;  //degrees     
            angle.z += temp.z;  //degrees     

        }
        printf("x: %lf, y: %lf, z: %lf, measurements: %d, i: %d\n", angle.x, angle.y, angle.z, numMeasurements, i);

        usleep(5 * 1000);
    }
}



void L3g4200d::ResetFIFO() {

    /* Set FIFO_CTRL_REG (0x2E)
    ====================================================================
    BIT  Symbol    Description                                   Default
    ---  ------    --------------------------------------------- -------
    7-5  FM        bypa(0), fifo(2), str(4), s2f(6), b2f(8)      0  
    4-0  WTM       Watermark level (num measurements in fifo)    -    */

    i2c->writeRegister(L3G4200D_REG_FIFO_CTRL_REG, 0x00);
    usleep(10 * 1000); //10 ms
    i2c->writeRegister(L3G4200D_REG_FIFO_CTRL_REG, 0x20);
}