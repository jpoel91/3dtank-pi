#ifndef ESC_H
#define ESC_H

#include <iostream>

class Esc {
    public:
        Esc(uint8_t position);
        ~Esc();

    private:
        uint8_t position;


};

#endif