/* https://github.com/jarzebski/Arduino-L3G4200D */
/* https://github.com/adafruit/Adafruit_L3GD20_U/ */

#ifndef L3G4200D_H
#define L3G4200D_H

#include "../communication/i2c.h"
#include <iostream>
#include <unistd.h>

#define L3G4200D_INCREMENTAL_READ   (0x80)

#define L3G4200D_ADDRESS            (0xD2 >> 1)
#define L3G4200D_REG_WHO_AM_I       (0x0F)

#define L3G4200D_REG_CTRL_REG1      (0x20)
#define L3G4200D_REG_CTRL_REG2      (0x21)
#define L3G4200D_REG_CTRL_REG3      (0x22)
#define L3G4200D_REG_CTRL_REG4      (0x23)
#define L3G4200D_REG_CTRL_REG5      (0x24)
#define L3G4200D_REG_REFERENCE      (0x25)
#define L3G4200D_REG_OUT_TEMP       (0x26)
#define L3G4200D_REG_STATUS_REG     (0x27)

#define L3G4200D_REG_OUT_X_L        (0x28)
#define L3G4200D_REG_OUT_X_H        (0x29)
#define L3G4200D_REG_OUT_Y_L        (0x2A)
#define L3G4200D_REG_OUT_Y_H        (0x2B)
#define L3G4200D_REG_OUT_Z_L        (0x2C)
#define L3G4200D_REG_OUT_Z_H        (0x2D)

#define L3G4200D_REG_FIFO_CTRL_REG  (0x2E)
#define L3G4200D_REG_FIFO_SRC_REG   (0x2F)

#define L3G4200D_REG_INT1_CFG       (0x30)
#define L3G4200D_REG_INT1_SRC       (0x31)
#define L3G4200D_REG_INT1_THS_XH    (0x32)
#define L3G4200D_REG_INT1_THS_XL    (0x33)
#define L3G4200D_REG_INT1_THS_YH    (0x34)
#define L3G4200D_REG_INT1_THS_YL    (0x35)
#define L3G4200D_REG_INT1_THS_ZH    (0x36)
#define L3G4200D_REG_INT1_THS_ZL    (0x37)
#define L3G4200D_REG_INT1_DURATION  (0x38)

/*Predefined numbers..
#define L3G4200D_SCALE_250DPS       (0.00875f)
#define L3G4200D_SCALE_500DPS       (0.0175f)
#define L3G4200D_SCALE_2000DPS      (0.07f)
*/

/* Determined values by test */
#define L3G4200D_SCALE_250DPS       (0.00875f)
#define L3G4200D_SCALE_500DPS       (0.0165F)
#define L3G4200D_SCALE_2000DPS      (0.07f)

/* CTRL REG1 SETTINGS */
#define L3G4200D_800HZ_110CO        (0xF0)
#define L3G4200D_ENABLE_ALL_AXIS    (0x0F)
#define L3G4200D_SHUTDOWN           (0x00)

/* EXCEPTIONS */
#define L3G4200D_NOT_FOUND          (0x10)

/* CALIBRATION */
#define L3G4200D_NUM_CAL            (200)

typedef struct Vector {
    int16_t x;
    int16_t y;
    int16_t z;
} Vector_t;

typedef struct DVector {
    double x;
    double y;
    double z;
} DVector_t;

typedef enum VectorEnum {
    x_l, x_h,
    y_l, y_h,
    z_l, z_h
} Vector_e;

class L3g4200d {
    public:
        L3g4200d();
        ~L3g4200d();
        void Init();
        void Calibrate();
        void Shutdown();
        void Boot();
        void Measure();

        
    private:
        void ResetFIFO();
        int32_t int16to32(int16_t num);
        IICCon* i2c;
        Vector_t calibrationValue;
        DVector_t noiseFilter;
        DVector_t angle;
};

#endif