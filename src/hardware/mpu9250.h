#ifndef MPU9250_H
#define MPU9250_H

#include <iostream>
#include "../communication/i2c.h"

//TODO: Add magneto meter.

#define MPU9250_I2C_ADDR        0x68
#define AK8963_I2C_ADDR         0x

#define MPU9250_ACCEL_XOUT_H    0x3B
#define MPU9250_ACCEL_XOUT_L    0x3C
#define MPU9250_ACCEL_YOUT_H    0x3D
#define MPU9250_ACCEL_YOUT_L    0x3E
#define MPU9250_ACCEL_ZOUT_H    0x3F
#define MPU9250_ACCEL_ZOUT_L    0x40

#define MPU9250_TEMP_OUT_H      0x41
#define MPU9250_TEMP_OUT_L      0x42

#define MPU9250_GYRO_XOUT_H     0x43
#define MPU9250_GYRO_XOUT_L     0x44
#define MPU9250_GYRO_YOUT_H     0x45
#define MPU9250_GYRO_YOUT_L     0x46
#define MPU9250_GYRO_ZOUT_H     0x47
#define MPU9250_GYRO_ZOUT_L     0x48

#define MPU9250_CONFIG          0x1A
#define MPU9250_GYRO_CONFIG     0x1B
#define MPU9250_ACCEL_CONFIG    0x1C
#define MPU9250_ACCEL_CONFIG2   0x1D

#define MPU9250_FIFO_EN         0x23
#define MPU9250_FIFO_COUNTH     0x72
#define MPU9250_FIFO_COUNTL     0x73
#define MPU9250_FIFO_R_W        0x74

#define MPU9250_WHO_AM_I        0x75

#define AK8963_MAG_XOUT_H       0x03
#define AK8963_MAG_XOUT_L       0x04
#define AK8963_MAG_YOUT_H       0x05
#define AK8963_MAG_YOUT_L       0x06
#define AK8963_MAG_ZOUT_H       0x07
#define AK8963_MAG_ZOUT_L       0x08

#define AK8963_ASAX             0x10
#define AK8963_ASAY             0x11
#define AK8963_ASAZ             0x12

#define AK8963_WHOAMI           0x00
#define AK8963_INFO             0x01
#define AK8963_ST1              0x02
#define AK8963_ST2              0x09
#define AK8963_CNTL             0x0A




typedef struct Vector {
    int16_t x;
    int16_t y;
    int16_t z;
} Vector_t;

typedef struct DVector {
    double x;
    double y;
    double z;
} DVector_t;

typedef enum VectorEnum {
    x_l, x_h,
    y_l, y_h,
    z_l, z_h
} Vector_e;

class Mpu9250 {
    public:
        Mpu9250(uint8_t gyro_acc_addr);
        ~Mpu9250();
        void Init();
        void Calibrate();
        void Shutdown();
        void Boot();
        void Measure();
    private:
        void ResetFIFO();
        IICCon* gyro_acc;
        IICCon* mag;
        Vector_t calibrationValue;
        DVector_t noiseFilter;
        DVector_t angle;
};

#endif