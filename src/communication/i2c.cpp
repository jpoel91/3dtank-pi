#include <iostream>     //cout
#include "stdio.h"      //printf
#include <fcntl.h>      //O_RDWD define
#include <unistd.h>     //write
#include <sys/ioctl.h>  //ioctl
#include <stdlib.h>     //malloc
#include "i2c.h"

IICCon::IICCon(uint8_t addr) {
    
    // Check if I2C bus can be reached.
    if ((fd = open(IICBUS, O_RDWR)) < 0) {
        printf("Can't open: (%s) on address 0x%02X......\n", IICBUS, addr);
        connected = 0;
        this->fd = -1;
        this->addr = 0;
        throw IICEXCEPTION;
    }else{
        printf("(%s) connected on address 0x%02x.\n", IICBUS, addr);
        this->fd = fd;
        this->addr = addr;
        connected = 1;
    }

    
}

IICCon::~IICCon() {
    if (this->fd != -1) {
        printf("I2C (0x%02x) connection closed.\n", addr);
        close(this->fd);
    }
}

int IICCon::writeRegister(uint8_t reg, uint8_t* data, int length) {

    int buffsize = length + 1;

    if (!isConnected()) return -1;
    if (ioctl(fd, PI_SLAVE_ADDR, addr) < 0 ) return -1;
    
    uint8_t* buff = (uint8_t*)malloc(buffsize * sizeof(uint8_t));

    //Format new buffer.
    buff[0] = reg;
    for (int i = 1; i < buffsize; i++) {
        buff[i] = data[i - 1];
    }
    
    //Write data
    int status = write(this->fd, buff, buffsize);

    free(buff);
    return status;
}

int IICCon::writeRegister(uint8_t reg, uint8_t data) {
    //printf("Write 0x%02X to 0x%02X.\n", reg, data);
    return writeRegister(reg, &data, 1);
}   

uint8_t* IICCon::readRegister(uint8_t reg, int bytes) {

    if (!isConnected()) return NULL;
    if (ioctl(this->fd, PI_SLAVE_ADDR, this->addr) < 0) return NULL;
    
    uint8_t *readbuff = (uint8_t*)malloc(bytes * sizeof(uint8_t));

    if (write(this->fd, &reg, 1) != 1)  return NULL;

    int status = read(this->fd, readbuff, bytes);

    //printf("bytes read: %d\n", status);

    return readbuff;    
}

uint8_t IICCon::readRegister(uint8_t reg) {
    uint8_t* ret = readRegister(reg, 1);

    if (ret == NULL) {
        printf("MALLOC FAULTED!\n");
        return -1;
    }
    uint8_t retval = ret[0];
    free(ret);
    return retval;
}

bool IICCon::isConnected() {
    if (this->connected && this->fd != -1) {
        return true;
    }
    printf("Connection with (%s) lost..\n", IICBUS);
    return false;
}