#ifndef I2C_H
#define I2C_H

#define IICBUS "/dev/i2c-1"
#define PI_SLAVE_ADDR 0x0703

//i2c exceptions
#define IICEXCEPTION 0x01

typedef unsigned char uint8_t;

class IICCon  {
    public:
        IICCon(uint8_t addr);
        ~IICCon();
        int writeRegister(uint8_t reg, uint8_t* data, int length);
        int writeRegister(uint8_t reg, uint8_t data);
        uint8_t* readRegister(uint8_t reg, int bytes);
        uint8_t readRegister(uint8_t reg);
        bool isConnected();

    private:
        int fd;
        uint8_t addr;
        bool connected;
        
};

#endif