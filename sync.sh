#!/bin/sh

IPADDRESS=10.0.0.11

if [ $# -gt 0 ]; then
    echo $#
    echo "stopped!"
    ssh pi@$IPADDRESS 'killall motorpi'
    return
fi

    echo "IP Adres:"
    echo $IPADDRESS
    make clean
    make
    rsync -va --exclude '*.o' -e ssh * pi@$IPADDRESS:/home/pi/3dtank --delete
    #ssh pi@$IPADDRESS 'cd motorpi && ./build/motorpi'
    make clean
    echo "Done!"
